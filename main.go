package main

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/labstack/gommon/log"
)

var port = "8080"

func main() {
	log.Fatal(http.ListenAndServe(":"+port, router()))
}

func router() http.Handler {
	r := mux.NewRouter()
	r.HandleFunc("/", index)
	r.HandleFunc("/greeting", greet)
	r.HandleFunc("/ping", healthCheck)
	return r
}

func greet(w http.ResponseWriter, _ *http.Request) {
	p := Payload{
		Message: "Hi there",
		Status:  true,
		Code:    200,
	}
	resp, err := json.Marshal(p)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	_, _ = w.Write(resp)
}

type Payload struct {
	Message string
	Status  bool
	Code    int
}

func index(w http.ResponseWriter, r *http.Request) {
	p := Payload{
		Message: "Welcome to homepage",
		Status:  true,
		Code:    200,
	}
	resp, err := json.Marshal(p)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	_, _ = w.Write(resp)
}

func healthCheck(w http.ResponseWriter, r *http.Request) {
	p := Payload{
		Message: "Good",
		Status:  true,
		Code:    200,
	}
	resp, err := json.Marshal(p)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	_, _ = w.Write(resp)
}
