package main

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func Test_healthCheck(t *testing.T) {
	req, err := http.NewRequest("GET", "/ping", nil)

	if err != nil {
		t.Fatal(err)
	}

	w := httptest.NewRecorder()
	handler := http.HandlerFunc(healthCheck)
	handler.ServeHTTP(w, req)

	status := w.Code
	if status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}

	expected := `{"Message":"Good","Status":true,"Code":200}`

	if w.Body.String() != expected {
		t.Errorf("handler returned wrong body: got %v want %v", w.Body.String(), expected)
	}
}

func Test_router(t *testing.T) {
	w := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodGet, "/greeting", nil)
	router().ServeHTTP(w, req)

	expected := `{"Message":"Hi there","Status":true,"Code":200}`
	actual := w.Body.String()

	if expected != actual {
		t.Fatalf("Expected %s but got %s", expected, actual)
	}
}

func Test_index(t *testing.T) {
	req, err := http.NewRequest("GET", "/", nil)

	if err != nil {
		t.Fatal(err)
	}

	w := httptest.NewRecorder()
	handler := http.HandlerFunc(index)
	handler.ServeHTTP(w, req)

	status := w.Code
	if status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}
}

func Test_greet(t *testing.T) {
	req, err := http.NewRequest("GET", "/greeting", nil)

	if err != nil {
		t.Fatal(err)
	}

	w := httptest.NewRecorder()
	handler := http.HandlerFunc(greet)
	handler.ServeHTTP(w, req)

	status := w.Code
	if status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}

	expected := `{"Message":"Hi there","Status":true,"Code":200}`

	if w.Body.String() != expected {
		t.Errorf("handler returned wrong body: got %v want %v", w.Body.String(), expected)
	}
}
